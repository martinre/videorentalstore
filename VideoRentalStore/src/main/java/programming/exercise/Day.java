package programming.exercise;

/**
 * 
 * @author Martin
 *
 * A class, which basically visualizes the time passed a rent/rents.
 */
public class Day {
	private int daysPassed;

	public Day(int daysPassed) {
		super();
		this.daysPassed = daysPassed;
	}

	public int getDaysPassed() {
		return daysPassed;
	}
}
