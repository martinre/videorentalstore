package programming.exercise;

import java.util.ArrayList;
/**
 * 
 * @author Martin
 *
 */
public class Customer {
	private ArrayList<Rent> filmRents;
	private int bonusPoints;
	private int daysPassed;
	
	/*
	 * Constructor for Customer.
	 */
	public Customer() {
		super();
		this.filmRents = new ArrayList<Rent>();
		this.bonusPoints = 0;
		this.daysPassed = 0;
	}
	/*
	 * All the Customer's rents will go to this Array.
	 */
	public ArrayList<Rent> getFilmRents() {
		return filmRents;
	}
	/*
	 * The setBonusPoints setter is needed for the Main class to visualize paying with Bonus points.
	 * As one day rent needs 25 Bonus points then reaching for that number quickly it's reasonable to use a setter.
	 */
	public void setBonusPoints(int bonusPoints) {
		this.bonusPoints = bonusPoints;
	}
	/*
	 * The getBonusPoints getter is for Testing purpose.
	 */
	public int getBonusPoints() {
		return bonusPoints;
	}
	public void setDaysPassed(int daysPassed) {
		this.daysPassed = daysPassed;
	}

	public void updateBonusPoints(){
		if(getFilmRents().get(getFilmRents().size() - 1).getFilm().getType().equals("New release")){
			this.bonusPoints += 2;
		}
		else{
			this.bonusPoints += 1;
		}
	}
	
	public void printRentalReceipt(){
		int total = 0;
		int totalLate = 0;
		String filmsReturned = "";
		String filmsReturnedBonus = "";
		String filmsReturnedLate = "";
		for(Rent rent : filmRents){
			rent.setRentDaysPassed(daysPassed);
			if(!rent.isPaidWithBonusPoints()){
				filmsReturned += (rent.getFilm().getName() + " (" + rent.getFilm().getType() + ") " + rent.getRentDays() + " days " + rent.getPrice() + " EUR\n");
				total += rent.getPrice();
			}
			else{
				int bonusesPaid = 25 * rent.getRentDays();
				filmsReturnedBonus += (rent.getFilm().getName() + " (" + rent.getFilm().getType() + ") " + rent.getRentDays() + " days " + "(Paid with " +  bonusesPaid + " Bonus points)");
				this.bonusPoints -= bonusesPaid;
			}
			if(rent.getRentDays() < daysPassed){
				filmsReturnedLate += (rent.getFilm().getName() + " (" + rent.getFilm().getType() + ") " + (daysPassed - rent.getRentDays()) + " extra days " + rent.getPriceLate() + " EUR\n");
				totalLate += rent.getPriceLate();
			}
		}
		if(filmsReturned.length() > 0){
			System.out.println(filmsReturned.substring(0, filmsReturned.length()-1));
			System.out.println("Total price: " + total + " EUR\n");
		}
		if(filmsReturnedLate.length() > 0){
			System.out.println(filmsReturnedLate.substring(0, filmsReturnedLate.length()-1));
			System.out.println("Total late charge: " + totalLate + " EUR\n");
		}
		if(filmsReturnedBonus.length() > 0){
			System.out.println(filmsReturnedBonus);
			System.out.println("Remaining Bonus Points: " + bonusPoints + "\n");
		}
		
	}

}
