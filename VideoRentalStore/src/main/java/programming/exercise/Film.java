package programming.exercise;

/**
 * 
 * @author Martin
 *
 */
public class Film {
	
	private String name;
	private String type;

	public Film(String name, String type) {
		super();
		this.name = name;
		this.type = type;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getName() {
		return name;
	}
}
