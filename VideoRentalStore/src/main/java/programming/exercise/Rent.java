package programming.exercise;

/**
 * 
 * @author Martin
 *
 */
public class Rent {
	public static final int PREMIUM_PRICE = 4;
	public static final int BASIC_PRICE = 3;
	
	private Film film;
	private int rentDays;
	private int price;
	private int priceLate;
	private boolean paidWithBonusPoints;
	private int rentDaysPassed;
	
	public Rent(Film film, int rentDays, boolean paidWithBonusPoints) {
		super();
		this.film = film;
		this.rentDays = rentDays;
		this.price = priceCalculator(film.getType(), rentDays);
		this.paidWithBonusPoints = paidWithBonusPoints;
		this.rentDaysPassed = 0;
	}
	
	public Film getFilm() {
		return film;
	}
	
	public int getRentDays() {
		return rentDays;
	}
	
	public int getPrice() {
		return price;
	}
	
	public int getPriceLate() {
		return priceLate;
	}

	public boolean isPaidWithBonusPoints() {
		return paidWithBonusPoints;
	}
	
	public void setRentDaysPassed(int rentDaysPassed) {
		this.rentDaysPassed = rentDaysPassed;
		this.priceLate = priceCalculatorLate(film.getType());
	}

	public int priceCalculator(String type, int days){
		int price = 0;

		if(type.equals("New release")){
			price = days * PREMIUM_PRICE;
		}
		else if(type.equals("Regular rental")){
			if(days > 3){
				price = BASIC_PRICE + (days - 3) * BASIC_PRICE;
			}
			else{
				price = BASIC_PRICE;
			}
		}
		else if(type.equals("Old film")){
			if(days > 5){
				price = BASIC_PRICE + (days - 5) * BASIC_PRICE;
			}
			else{
				price = BASIC_PRICE;
			}
		}
		
		
		return price;
	}
	
	public int priceCalculatorLate(String type){
		int price = 0;
		
		if(rentDaysPassed > rentDays){
			if(type.equals("New release")){
				price = (rentDaysPassed - rentDays) * PREMIUM_PRICE;
			}
			else if(type.equals("Regular rental") || type.equals("Old film")){
				price = (rentDaysPassed - rentDays) * BASIC_PRICE; 
			}
		}
		return price;
	}
	
}
