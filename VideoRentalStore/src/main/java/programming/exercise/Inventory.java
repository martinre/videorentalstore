package programming.exercise;

import java.util.ArrayList;
import java.util.List;
/**
 * 
 * @author Martin
 *
 */
public class Inventory {
	private List<Film> allFilms;
	private List<Film> filmsInStore;
	private ArrayList<Rent> rents;
	
	public Inventory(){
		this.allFilms = new ArrayList<Film>();
		this.filmsInStore = new ArrayList<Film>();
		this.rents = new ArrayList<Rent>();
	}
	
	public List<Film> getAllFilms() {
		return allFilms;
	}

	public List<Film> getFilmsInStore() {
		return filmsInStore;
	}
	
	public ArrayList<Rent> getRents() {
		return rents;
	}
	
	public void addFilm(Film filmAdd){
		if(!listAllFilms().contains(filmAdd.getName()) && !listAllFilms().contains(filmAdd.getType())){
			this.allFilms.add(filmAdd);
			this.filmsInStore.add(filmAdd);
		}
	}
	
	public void removeFilm(Film filmRemove){
		this.allFilms.remove(filmRemove);
		this.filmsInStore.remove(filmRemove);
	}
	
	public ArrayList<String> listAllFilms(){
		return listFilms(allFilms);
	}
	
	public ArrayList<String> listFilmsInStore(){
		return listFilms(filmsInStore);
	}
	
	public ArrayList<String> listFilms(List<Film> filmsList){
		ArrayList<String> films = new ArrayList<String>();
		for(int i = 0; i < filmsList.size(); i++){
			films.add(filmsList.get(i).getName());
		}
		return films;
	}
	
	public void ChangeFilmType(String filmName, String newType){
		for(Film f : allFilms){
			if(f.getName().equals(filmName)){
				f.setType(newType);
			}
		}
		for(Film f : filmsInStore){
			if(f.getName().equals(filmName)){
				f.setType(newType);
			}
		}
	}
	
	
}
